Proyecto del curso de PHP de junio de 2016 proteco

Notas:
    base.sql: es la script para crear la base de datos 'chocolates'
    proyecto-prueba.sql: es un script que exporte de las pruebas que estaba realizando

Importante: 
    para poner a funcionar en local se debe considerar el archivo
        
```
#!php
   /scripts/conexion.php
```
  que contiene las credenciales de conexion a la base de datos


Autor y Titular: Sánchez Baños Margarito