<?php 
    session_start();
    unset($_SESSION['user']);
    unset($_SESSION['rol']);
    $_SESSION['autenticado']=false;
    header('Location: ../index.php');