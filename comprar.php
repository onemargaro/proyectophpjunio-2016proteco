<?php 
    include 'scripts/conexion.php';
    $id = $_GET['chocolate'];
    $query = "SELECT * FROM chocolate WHERE id=$id";
    $resultado = $c->query($query);
    $chocolate = $resultado->fetch_array(MYSQLI_ASSOC);
?>
<!DOCTYPE html>
<html>
<head>
    <title>Tienda de Chocolates</title>
    <link rel="stylesheet" type="text/css" href="bower_components/bootstrap/dist/css/bootstrap.css">
</head>
<body>
    <?php include 'partials/header.php'; ?>
    <div class="container">
            <div class="col-sm-4">
                <div class="card">
                  <img class="card-img-top img-fluid" src="data:image/jpg;base64,<?php echo base64_encode($chocolate['imagen']); ?> alt="foto">
                  <div class="card-block">
                    <h4 class="card-title"><?php echo $chocolate['nombre'];?></h4>
                    <p class="card-text"><?php echo $chocolate['descripcion'];?></p>
                    <p class="card-text"><?php echo "\$".$chocolate['precio'];?></p>
                  </div>
                </div>
            </div>
            <div class="col-sm-8">
                <div class="card">
                    <div class="card-block">
                        <?php if (isset($_SESSION['user'])) {
                            ?> 
                            <h4 class="card-title"><?php echo $_SESSION['user'];?></h4>
                            <form  method="POST">
                                <fieldset class="form-group">
                                    <label for="Cantidad">Cantidad</label>
                                    <input type="number" class="form-control" id="Cantidad" placeholder="Cantidad" name="Cantidad" required>
                                  </fieldset>
                                  <button type="submit" class="btn btn-secondary">Comprar</button>
                            </form>
                        <?php
                            }else{
                                ?>
                                    <h4 class="card-title">Registrate para poder comprar</h4>
                                <?php
                            } ?>
                    </div>
                </div>
                <div class="card">
                    <?php if (isset($_POST['Cantidad'])): ?>
                        <div class="card-block">
                            <h4 class="card-title">Confirmar</h4>
                            <form action="scripts/comprarChocolates.php" method="POST">
                                <input type="hidden" name="Cantidad" value="<?php echo $_POST['Cantidad']; ?>">
                                <input type="hidden" name="importe" value="<?php echo $_POST['Cantidad']*$chocolate['precio']; ?>">
                                <input type="hidden" name="chocolate_id" value="<?php echo $chocolate['id']; ?>">
                                <p>Cantidad: <?php echo $_POST['Cantidad']; ?></p>
                                <p>Precio: <?php echo $_POST['Cantidad']*$chocolate['precio']; ?></p>
                                <button type="submit" class="btn btn-success">Confirmar</button>
                            </form>
                        </div>
                    <?php endif ?>
                </div>
            </div>      
    </div>
    <?php include 'partials/footer.php'; ?>
    <script type="text/javascript" src="bower_components/jquery/dist/jquery.js"></script>
    <script type="text/javascript" src="bower_components/bootstrap/dist/js/bootstrap.js"></script>
</body>
</html>