<?php 
    include 'scripts/conexion.php';
    $query = "SELECT * FROM chocolate";
    $resultado = $c->query($query);
?>
<!DOCTYPE html>
<html>
<head>
    <title>Tienda de Chocolates</title>
    <link rel="stylesheet" type="text/css" href="bower_components/bootstrap/dist/css/bootstrap.css">
</head>
<body>
    <?php include 'partials/header.php'; ?>
    <div class="container">
        <?php if (isset($_SESSION['compra'])): ?>
            <?php if ($_SESSION['compra']): ?>
                <div class="compra-alert alert alert-success" role="alert">
                    Se realizo la compra satisfactoriamente
                </div>
            <?php else: ?>
                <div class="compra-alert alert alert-danger" role="alert">
                    Ha ocurrido un problema con la compra ;(
                </div>
            <?php endif ?>
        <?php endif ?>
        <?php while ($chocolate = $resultado->fetch_array(MYSQLI_ASSOC)) {
            ?>
            <div class="col-sm-4">
                <div class="card">
                  <img class="card-img-top img-fluid" src="data:image/jpg;base64,<?php echo base64_encode($chocolate['imagen']); ?> alt="foto">
                  <div class="card-block">
                    <h4 class="card-title"><?php echo $chocolate['nombre'];?></h4>
                    <p class="card-text"><?php echo $chocolate['descripcion'];?></p>
                    <p class="card-text"><?php echo "\$".$chocolate['precio'];?></p>
                    <a href="comprar.php?chocolate=<?php echo $chocolate['id']; ?>" class="btn btn-secondary">Comprar</a>
                        <?php if ($_SESSION['autenticado'] && $muestra): ?>
                            <a href="editarChocolate.php?id=<?php echo $chocolate['id']; ?>" class="btn btn-secondary">Editar</a>
                            <a href="scripts/eliminarChocolate.php?id=<?php echo $chocolate['id']; ?>" class="btn btn-danger">Eliminar</a>
                        <?php endif ?>
                  </div>
                </div>
            </div>
            <?php
        } ?>
        


    </div>
    <?php include 'partials/footer.php'; ?>
    <script type="text/javascript" src="bower_components/jquery/dist/jquery.js"></script>
    <script type="text/javascript" src="bower_components/bootstrap/dist/js/bootstrap.js"></script>
    <script type="text/javascript">
        setTimeout(function () {
            $('.compra-alert').css('display','none');
            <?php unset($_SESSION['compra']); ?>
        }, 3000);
    </script>
</body>
</html>