<?php 
    include 'scripts/conexion.php';
    $query = 'SELECT u.id as id,u.nombre as nombre, u.apellidos as apellidos, u.sexo as sexo, u.username as username, r.nombre as rol FROM usuario as u JOIN rol r ON u.rol_id=r.id';
    $resultado = $c->query($query);
?>
<!DOCTYPE html>
<html>
<head>
    <title>Tienda de Chocolates</title>
    <link rel="stylesheet" type="text/css" href="bower_components/bootstrap/dist/css/bootstrap.css">
</head>
<body>
    <?php include 'partials/header.php'; ?>
    <div class="container">
        <?php 
            while ($user = $resultado->fetch_array(MYSQLI_ASSOC)) {
            ?>
               <div class="col-md-4">
                    <div class="card card-block">
                        <h4 class="card-title">Nombre: <?php echo $user['nombre'];?></h4>
                        <h5>Apellidos: <?php echo $user['apellidos'];?></h5>
                        <h5>Sexo: <?php echo $user['sexo'];?></h5>
                        <h5>Username: <?php echo $user['username'];?></h5>
                        <h5>Rol: <?php echo $user['rol'];?></h5>
                        <?php if ($_SESSION['autenticado'] && $muestra): ?>
                            <a href="editarUsuario.php?id=<?php echo $user['id']; ?>" class="btn btn-secondary">Editar</a>
                            <a href="scripts/eliminarUsuario.php?id=<?php echo $user['id']; ?>" class="btn btn-danger">Eliminar</a>
                        <?php endif ?>
                    </div>
                </div> 
        <?php } ?>
    </div>
    <?php include 'partials/footer.php'; ?>
    <script type="text/javascript" src="bower_components/jquery/dist/jquery.js"></script>
    <script type="text/javascript" src="bower_components/bootstrap/dist/js/bootstrap.js"></script>
</body>
</html>