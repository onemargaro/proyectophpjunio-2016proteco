<?php 
    include 'scripts/conexion.php';
    $id = $_GET['id'];
    $query = "SELECT * FROM chocolate WHERE id=$id";
    $resultado = $c->query($query);
    $chocolate = $resultado->fetch_array(MYSQLI_ASSOC);
?>
<!DOCTYPE html>
<html>
<head>
    <title>Tienda de Chocolates</title>
    <link rel="stylesheet" type="text/css" href="bower_components/bootstrap/dist/css/bootstrap.css">
</head>
<body>
    <?php include 'partials/header.php'; 
      if (!isset($_SESSION['rol'])) {
        header('Location: index.php');
      }
      if (isset($_SESSION['rol'])) {
        if ($_SESSION['rol']!='Administrador') {
          header('Location: index.php');
        }
      }
    ?>
    <div class="container">
        <div class="col-sm-8 col-sm-offset-2">
            <div class="card card-block">
              <h4 class="card-title text-xs-center">Editar Chocolate</h4>
              <form action="scripts/actualizaChocolate.php" method="POST" enctype="multipart/form-data">
                  <input type="hidden" name="id" value="<?php echo $id; ?>">
                  <img class="card-img-top img-fluid" src="data:image/jpg;base64,<?php echo base64_encode($chocolate['imagen']); ?> alt="foto">
                    <label class="file">
                      <input type="file" id="file" name="imagen">
                      <span class="file-custom"></span>
                    </label>
                    <fieldset class="form-group">
                        <label for="nombre">Nombre</label>
                        <input type="text" class="form-control" id="nombre" placeholder="Nombre" name="nombre" value="<?php echo $chocolate['nombre']; ?>">
                      </fieldset>
                    <fieldset class="form-group">
                        <label for="Descripcion">Descripcion</label>
                        <textarea class="form-control" id="Descripcion" name="descripcion" placeholder="Descripcion" rows="3"><?php echo $chocolate['descripcion']; ?></textarea>
                      </fieldset>
                    <fieldset class="form-group">
                        <label for="Precio">Precio</label>
                        <input type="number" value="<?php echo $chocolate['precio']; ?>" class="form-control" id="Precio" placeholder="Precio" name="precio" >
                      </fieldset>
                    <button type="submit" class="btn btn-primary">Actualizar</button>
              </form>
            </div>
        </div>
    </div>
    <?php include 'partials/footer.php'; ?>
    <script type="text/javascript" src="bower_components/jquery/dist/jquery.js"></script>
    <script type="text/javascript" src="bower_components/bootstrap/dist/js/bootstrap.js"></script>
</body>
</html>