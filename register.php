<?php 
    include 'scripts/conexion.php';
    
    $query = "SELECT * FROM rol";
    $resultado = $c->query($query);
?>
<!DOCTYPE html>
<html>
<head>
    <title>Tienda de Chocolates</title>
    <link rel="stylesheet" type="text/css" href="bower_components/bootstrap/dist/css/bootstrap.css">
</head>
<body>
    <?php include 'partials/header.php'; ?>
    <div class="container">
        <div class="col-md-8 col-md-offset-2">
            <div class="card card-block">
              <h4 class="card-title">Registrarse</h4>
              <form action="scripts/registerUser.php" method="POST">
                <fieldset class="form-group">
                    <label for="Username">Username</label>
                    <input type="text" class="form-control" id="Username" placeholder="Username" name="username" required>
                  </fieldset>
                  <fieldset class="form-group">
                    <label for="Password">Password</label>
                    <input type="password" class="form-control" id="Password" placeholder="Password" name="password" required>
                  </fieldset>
                  <fieldset class="form-group">
                    <label for="nombre">Nombre</label>
                    <input type="text" class="form-control" id="nombre" placeholder="Nombre" name="nombre" required>
                  </fieldset>
                  <fieldset class="form-group">
                    <label for="Apellidos">Apellidos</label>
                    <input type="text" class="form-control" id="Apellidos" placeholder="Apellidos" name="apellidos" required>
                  </fieldset>
                  <fieldset class="form-group">
                    <label for="Sexo">Sexo</label>
                    <select class="form-control" name="sexo" id="Sexo">
                        <option value="H">Hombre</option>
                        <option value="M">Mujer</option>
                    </select>
                  </fieldset>
                <button type="submit" class="btn btn-primary">Guardar</button>
              </form>
            </div>
        </div>
    </div>
    <?php include 'partials/footer.php'; ?>
    <script type="text/javascript" src="bower_components/jquery/dist/jquery.js"></script>
    <script type="text/javascript" src="bower_components/bootstrap/dist/js/bootstrap.js"></script>
</body>
</html>