<?php 
    include 'scripts/conexion.php';
    $query = 'SELECT u.nombre as nombre, u.apellidos as apellidos, ch.nombre as chocolate, c.cantidad as cantidad, c.importe as importe FROM compra c join usuario u on c.usuario_id=u.id join chocolate ch on c.chocolate_id=ch.id;';
    $resultado = $c->query($query);
?>
<!DOCTYPE html>
<html>
<head>
    <title>Tienda de Chocolates</title>
    <link rel="stylesheet" type="text/css" href="bower_components/bootstrap/dist/css/bootstrap.css">
</head>
<body>
    <?php include 'partials/header.php'; ?>
    <div class="container">
        <table class="table table-hover">
            <thead>
                <tr>
                  <th>Nombre</th>
                  <th>Apellidos</th>
                  <th>Chocolate</th>
                  <th>Cantidad</th>
                  <th>Importe</th>
                </tr>
            </thead>
            <tbody>
                <?php while ($compra = $resultado->fetch_array(MYSQLI_ASSOC)) {
                    ?>
                    <tr>
                        <td><?php echo $compra['nombre'];?></td>
                        <td><?php echo $compra['apellidos'];?></td>
                        <td><?php echo $compra['chocolate'];?></td>
                        <td><?php echo $compra['cantidad'];?></td>
                        <td><?php echo $compra['importe'];?></td>
                    </tr>
                    <?php
                } ?>
            </tbody>
        </table>
    </div>
    <?php include 'partials/footer.php'; ?>
    <script type="text/javascript" src="bower_components/jquery/dist/jquery.js"></script>
    <script type="text/javascript" src="bower_components/bootstrap/dist/js/bootstrap.js"></script>
</body>
</html>