CREATE TABLE rol(
    id integer primary key not null auto_increment,
    nombre varchar(255),
    descripcion varchar(255)
);
CREATE TABLE usuario(
    id integer primary key not null auto_increment,
    nombre varchar(255),
    apellidos varchar(255),
    sexo char(1),
    username varchar(255),
    password varchar(255),
    rol_id integer not null,
    constraint foreign key (rol_id) references rol(id)
);
CREATE TABLE chocolate(
    id integer primary key not null auto_increment,
    imagen blob,
    nombre varchar(255),
    descripcion varchar(255),
    precio FLOAT(10,2)
);
CREATE TABLE compra(
    id integer primary key not null auto_increment,
    usuario_id integer not null,
    chocolate_id integer not null,
    cantidad integer,
    importe FLOAT(10,2),
    constraint foreign key (usuario_id) references usuario(id),
    constraint foreign key (chocolate_id) references chocolate(id)
);

INSERT INTO rol(nombre, descripcion) VALUES('Administrador','Rol de administrador');
INSERT INTO rol(nombre, descripcion) VALUES('Cliente','Rol de cliente');

INSERT INTO usuario(username,password,nombre,apellidos,sexo, rol_id) VALUES('onemargaro','margaro123','Margarito','Sánchez','H',1);
INSERT INTO usuario(username,password,nombre,apellidos,sexo, rol_id) VALUES('luisito','luis123','Luis','Torres','H',2);