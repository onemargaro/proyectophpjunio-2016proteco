<?php 
    include 'scripts/conexion.php';
    $roles_query = "SELECT * FROM rol";
    $id = $_GET['id'];
    $user_query = "SELECT * FROM usuario WHERE id = $id";
    $user = $c->query($user_query);
    $usuario = $user->fetch_array(MYSQLI_ASSOC);
    $roles = $c->query($roles_query);
?>
<!DOCTYPE html>
<html>
<head>
    <title>Tienda de Chocolates</title>
    <link rel="stylesheet" type="text/css" href="bower_components/bootstrap/dist/css/bootstrap.css">
</head>
<body>
    <?php include 'partials/header.php'; 
    if (!isset($_SESSION['rol'])) {
        header('Location: index.php');
      }
      if (isset($_SESSION['rol'])) {
        if ($_SESSION['rol']!='Administrador') {
          header('Location: index.php');
        }
      }
    ?>
    <div class="container">
          <div class="card card-block">
            <h4 class="card-title">Editar Usuario</h4>
            <form action="scripts/actualizaUsuario.php" method="POST">
              <input type="hidden" name="id" value='<?php echo $id; ?>'>
              <fieldset class="form-group">
                  <label for="Username">Username</label>
                  <input type="text" class="form-control" id="Username" placeholder="Username" name="username" value="<?php echo $usuario['username']; ?>">
                </fieldset>
                <fieldset class="form-group">
                  <label for="Password">Password</label>
                  <input type="password" class="form-control" value="<?php echo $usuario['password']; ?>" id="Password" placeholder="Password" name="password">
                </fieldset>
                <fieldset class="form-group">
                  <label for="nombre">Nombre</label>
                  <input type="text" value="<?php echo $usuario['nombre']; ?>" class="form-control" id="nombre" placeholder="Nombre" name="nombre">
                </fieldset>
                <fieldset class="form-group">
                  <label for="Apellidos">Apellidos</label>
                  <input type="text" class="form-control" id="Apellidos" placeholder="Apellidos" name="apellidos" value="<?php echo $usuario['apellidos']; ?>">
                </fieldset>
                <fieldset class="form-group">
                  <label for="Sexo">Sexo</label>
                  <select class="form-control" value="<?php echo $usuario['sexo']; ?>" name="sexo" id="Sexo">
                      <option value="H">Hombre</option>
                      <option value="M">Mujer</option>
                  </select>
                </fieldset>
                <fieldset class="form-group">
                  <label for="Rol">Rol</label>
                  <select class="form-control" value='<?php echo $usuario['rol_id']; ?>' name="rol_id" id="Rol">
                      <?php while ($rol = $roles->fetch_array(MYSQLI_ASSOC)) {
                         ?> 
                          <option value="<?php echo $rol['id']; ?>">
                              <?php echo $rol['nombre']; ?>
                          </option>
                      <?php  
                      } ?>
                  </select>
                </fieldset>
              <button type="submit" class="btn btn-primary">Actualizar</button>
            </form>
          </div>
    </div>
    <?php include 'partials/footer.php'; ?>
    <script type="text/javascript" src="bower_components/jquery/dist/jquery.js"></script>
    <script type="text/javascript" src="bower_components/bootstrap/dist/js/bootstrap.js"></script>
</body>
</html>