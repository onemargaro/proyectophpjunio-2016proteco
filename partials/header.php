<?php 
  session_start();
  if (isset($_SESSION['rol'])) {
    if ($_SESSION['rol']=="Administrador") {
      $muestra = true;
    }else{
      $muestra=false;
    }
  }else{
    $_SESSION['autenticado'] = false;
  }
?>
<nav class="navbar navbar-light bg-faded">
  <a class="navbar-brand" href="index.php">Chocolates</a>
  <ul class="nav navbar-nav">
    <li class="nav-item active">
      <a class="nav-link" href="index.php">Home <span class="sr-only">(current)</span></a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="usuarios.php">Usuarios</a>
    </li>
    <?php if ($_SESSION['autenticado'] && $muestra): ?>
      <li class="nav-item">
        <a class="nav-link" href="listaCompras.php">Compras</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="admin.php">Admin</a>
      </li>
    <?php endif ?>
  </ul>
  <?php if (!$_SESSION['autenticado']): ?>
    <form class="form-inline pull-xs-right" method="POST" action="scripts/login.php" >
        <div class="form-group">
          <label class="sr-only" for="Username">Username</label>
          <input type="text" name="username" class="form-control" id="Username" placeholder="Username">
        </div>
        <div class="form-group">
          <label class="sr-only" for="password">Password</label>
          <input type="password" name="password" class="form-control" id="password" placeholder="Password">
        </div>
      <button type="submit" class="btn btn-secondary">Sign in</button>
      <a href="register.php" class="btn btn-secondary">Register</a>
    </form>
  <?php else: ?>
    <form class="form-inline pull-xs-right" method="POST" action="scripts/logout.php" >
      <?php echo $_SESSION['user']; ?>
      <button type="submit" class="btn btn-secondary">Log out</button>
    </form>
  <?php endif ?>
</nav>