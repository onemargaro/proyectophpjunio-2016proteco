<?php 
    include 'scripts/conexion.php';
    
    $query = "SELECT * FROM rol";
    $resultado = $c->query($query);
?>
<!DOCTYPE html>
<html>
<head>
    <title>Tienda de Chocolates</title>
    <link rel="stylesheet" type="text/css" href="bower_components/bootstrap/dist/css/bootstrap.css">
</head>
<body>
    <?php include 'partials/header.php'; ?>
    <div class="container">
        <div class="col-md-6">
            <div class="card card-block">
              <h4 class="card-title">Agregar Chocolate</h4>
              <form action="scripts/agregaChocolate.php" method="POST" enctype="multipart/form-data">
                    <label class="file">
                      <input type="file" id="file" name="imagen">
                      <span class="file-custom"></span>
                    </label>
                    <fieldset class="form-group">
                        <label for="nombre">Nombre</label>
                        <input type="text" class="form-control" id="nombre" placeholder="Nombre" name="nombre" required>
                      </fieldset>
                    <fieldset class="form-group">
                        <label for="Descripcion">Descripcion</label>
                        <textarea class="form-control" id="Descripcion" name="descripcion" placeholder="Descripcion" rows="3"></textarea>
                      </fieldset>
                    <fieldset class="form-group">
                        <label for="Precio">Precio</label>
                        <input type="number" class="form-control" id="Precio" placeholder="Precio" name="precio" required>
                      </fieldset>
                    <button type="submit" class="btn btn-primary">Guardar</button>
              </form>
            </div>
        </div>
        <div class="col-md-6">
            <div class="card card-block">
              <h4 class="card-title">Agregar Usuario</h4>
              <form action="scripts/agregaUsuario.php" method="POST">
                <fieldset class="form-group">
                    <label for="Username">Username</label>
                    <input type="text" class="form-control" id="Username" placeholder="Username" name="username" required>
                  </fieldset>
                  <fieldset class="form-group">
                    <label for="Password">Password</label>
                    <input type="password" class="form-control" id="Password" placeholder="Password" name="password" required>
                  </fieldset>
                  <fieldset class="form-group">
                    <label for="nombre">Nombre</label>
                    <input type="text" class="form-control" id="nombre" placeholder="Nombre" name="nombre" required>
                  </fieldset>
                  <fieldset class="form-group">
                    <label for="Apellidos">Apellidos</label>
                    <input type="text" class="form-control" id="Apellidos" placeholder="Apellidos" name="apellidos" required>
                  </fieldset>
                  <fieldset class="form-group">
                    <label for="Sexo">Sexo</label>
                    <select class="form-control" name="sexo" id="Sexo">
                        <option value="H">Hombre</option>
                        <option value="M">Mujer</option>
                    </select>
                  </fieldset>
                  <fieldset class="form-group">
                    <label for="Rol">Rol</label>
                    <select class="form-control" name="rol_id" id="Rol">
                        <?php while ($rol = $resultado->fetch_array(MYSQLI_ASSOC)) {
                           ?> 
                            <option value="<?php echo $rol['id']; ?>">
                                <?php echo $rol['nombre']; ?>
                            </option>
                        <?php  
                        } ?>
                    </select>
                  </fieldset>
                <button type="submit" class="btn btn-primary">Guardar</button>
              </form>
            </div>
        </div>
    </div>
    <?php include 'partials/footer.php'; ?>
    <script type="text/javascript" src="bower_components/jquery/dist/jquery.js"></script>
    <script type="text/javascript" src="bower_components/bootstrap/dist/js/bootstrap.js"></script>
</body>
</html>